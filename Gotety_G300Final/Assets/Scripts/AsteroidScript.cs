﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    private Player player;

    public int score = 5;

    public float speed = 5f;
    public float rotate_speed = 50f;

    float randAngle;
    public float radius;

    Rigidbody2D rb;

    public bool canShoot;
    public bool canRotate;

    public float bound_Y = -11f;

    private Animator anim;
    private AudioSource explosionSound;


   

    void Awake()
    {
        anim = GetComponent<Animator>();
        explosionSound = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        if (canRotate)
        {
            if (Random.Range(0, 2) > 0)
            {
                rotate_speed = Random.Range(rotate_speed, rotate_speed + 20f);
                rotate_speed *= -1;
            }

            
        }
        randAngle = Random.Range(0f, 360f);

        SetPosition();
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        RotateEnemy();
    }


    void Move()
    {
        /**Vector3 temp = transform.position;
        temp.y -= speed * Time.deltaTime;
        transform.position = temp;

        if (temp.y < bound_Y)
        {
            gameObject.SetActive(false);
        } **/

        radius -= speed * Time.deltaTime;
        SetPosition();
    }

    void RotateEnemy()
    {
        if (canRotate) {
            transform.Rotate(new Vector3(0f, 0f, rotate_speed * Time.deltaTime), Space.World);
                }
    }

    void SetPosition()
    {

        float rad = randAngle * Mathf.Deg2Rad;

        Vector3 position = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
        position *= radius;

        // print(position + "angle:" + randAngle);

        rb.MovePosition(position);

        Vector3 euler = new Vector3(0f, 0f, -(randAngle - 180));
        rb.MoveRotation(Quaternion.Euler(euler));


        if (radius <= 0)
            gameObject.SetActive(false);



    }



    void TurnOffGameObject()
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            player.UpdateScore(score);
            //print("enemy attacked");
            //print(other.gameObject.name);
            explosionSound.Play();
            //CancelInvoke("StartShooting");
            Invoke("TurnOffGameObject", 5f);
            anim.Play("explosion1");
        }

    }
}
