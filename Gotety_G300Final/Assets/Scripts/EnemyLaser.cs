﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLaser : MonoBehaviour
{

    public float speed = 5f;
    public float deactivate_Timer = 3f;

    // Start is called before the first frame update
    void Start()
    {
        speed *= -1;


        //Invoke("DeactivateGameObject", deactivate_Timer);

        
    }

    // Update is called once per frame
    void Update()
    {
        Move();    
    }

    void Move() {
        Vector3 temp = transform.position;
        temp += speed * Time.deltaTime * transform.up;
        transform.position = temp;

        if ((transform.position.x >= 46 || transform.position.x <= -46) && (transform.position.y >= 21 || transform.position.y <= -21)) 
        {
            //print("out of bounds");
            gameObject.SetActive(false);
        }
    }

    /**void DeactivateGameObject()
    {
        if (Time.deltaTime == deactivate_Timer)
        {
            //print("enemy dead");
            gameObject.SetActive(false);
        }
    }
    **/

    void OnTriggerEnter2d(Collider2D other)
    {
        if (other.tag == "Player")
        {
            

            print("attacked");
        }
    }
}

