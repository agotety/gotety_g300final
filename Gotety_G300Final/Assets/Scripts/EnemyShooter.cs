﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    private Player player;
    public float speed = 5f;

    public int score = 10;

    float randAngle;
    public float angle2;
    public float radius;

    public float bound_Y = -11f;
    Rigidbody2D rb;

    bool reverse; 

    public Transform attack_point;
    public GameObject bulletPrefab;

    private Animator anim;
    public AudioSource explosionSound;
    public AudioSource shootEnemy;


    // Start is called before the first frame update

    void Awake()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();



    }
    void Start()
    {

        Invoke("StartShooting", Random.Range(1f, 3f));

        rb = GetComponent<Rigidbody2D>();

        randAngle = Random.Range(0f, 360f);
        angle2 = randAngle;

        SetPosition();
        //angle += randAngle * speed * Time.deltaTime;
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        //Vector3 temp = transform.position;
        //temp.y -= speed * Time.deltaTime;
        //transform.position = temp;

        /**
         * float randAngle = Random.Range(0f, 360f);

        angle += randAngle * speed * Time.deltaTime;
        float rad = angle * Mathf.Deg2Rad;

        Vector3 position = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
        //position *= radius;

        rb.MovePosition(position);

        if (position.y < bound_Y)
            gameObject.SetActive(false);
        
         **/
        

        if (!reverse)
        {
            SetPosition();
            radius -= speed * Time.deltaTime;
            if (radius <= 3.5f)
            {
                reverse = true;
                //gameObject.SetActive(false);
                radius = 3.5f;
                rb.SetRotation(-randAngle);
                Invoke("StartShootingReverse", Random.Range(1f, 3f));
            }
        }
        


    }

    void SetPosition() {

        //while (randAngle != angle2)
        {

            float rad = randAngle * Mathf.Deg2Rad;

            Vector3 position = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
            position *= radius;

            // print(position + "angle:" + randAngle);

            rb.MovePosition(position);

            Vector3 euler = new Vector3(0f, 0f, -(randAngle - 180));
            rb.MoveRotation(Quaternion.Euler(euler));

            //print("radius" + radius);
            //print("position" + position);

        }
        



    }

    void StartShooting()
    {

        shootEnemy.Play();
        GameObject bullet = Instantiate(bulletPrefab, attack_point.position, transform.rotation);
        //bullet.GetComponent<EnemyLaser>().isEnemy

        Invoke("StartShooting", Random.Range(0.25f, 1.5f));

    }

    void StartShootingReverse()
    {

        shootEnemy.Play();
        GameObject bullet = Instantiate(bulletPrefab, attack_point.position, transform.rotation);
        //bullet.GetComponent<EnemyLaser>().isEnemy

        Invoke("StartShootingReverse", Random.Range(0.25f, 1.5f));

    }

    void TurnOffGameObject()
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            player.UpdateScore(score);
            anim.Play("explosion2");
            //print("enemy attacked");
            //print(other.gameObject.name);
            explosionSound.Play();
            CancelInvoke("StartShooting");
            CancelInvoke("StartShootingReverse");
            Invoke("TurnOffGameObject", 5f);

        }

    }


}

   
