﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    Rigidbody2D rb;
    public float speed;
    public float speedMain;
    public float laserSpeed;
    public GameObject laserPrefab;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI livesText;
    //public GameObject winTextObject;
    //public GameObject loseTextObject;

    public int life = 5;
    public int score = 0;

    float angle;
    public float radius;

    public AudioSource shootAudio;
    public AudioSource damageAudio;

    // Start is called before the first frame update
    void Start()
    {
        speedMain = speed;
        rb = GetComponent<Rigidbody2D>();
        //shootAudio = GetComponent<AudioSource>();

        SetScoreText();
        SetLifeText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        //rb.velocity = new Vector2(horizontalInput * speed * Time.deltaTime, rb.velocity.y);

        radius += verticalInput * speed * Time.deltaTime *0.1f;

        if (radius > 13f) {
            radius = 13 ;
        }

        if (radius <= 0.5f)
        {
            radius = 0.5f;
        }

        angle += horizontalInput * speed * Time.deltaTime;
        if (angle < 0)
        {
            angle = 360 + angle;
        }

        if (angle >= 360)
        {
            angle -= 360;
        }
        float rad = angle * Mathf.Deg2Rad;

        //print(angle);
        Vector3 position = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
        position *= radius;

        rb.MovePosition(position);

        Vector3 euler = new Vector3(0f, 0f, -(angle-180));
        rb.MoveRotation(Quaternion.Euler(euler));
        
        //transform.LookAt(Vector3.zero);



    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shootAudio.Play();
            GameObject laser = Instantiate(laserPrefab, transform.position, transform.rotation);
            //Debug.Log("Shoot");
            Rigidbody2D laserRB = laser.GetComponent<Rigidbody2D>();
            laserRB.velocity = transform.up * laserSpeed;
            
        }
    }

    public void UpdateScore( int s)
    {
        score += s;
        SetScoreText();
    }

    void SetScoreText()
    {
        scoreText.text = "Score: " + score.ToString();

        if (score >= 2500)
        {
            // Set the text value of your 'winText'
            //winTextObject.SetActive(true);
            print("you win");
            SceneManager.LoadScene(2);
        }
    }

    void SetLifeText()
    {
        livesText.text = "Lives: " + life.ToString();

        if (life <= 0)
        {
            //Debug.Log("you lose bitch");
            SceneManager.LoadScene(3);
            //loseTextObject.SetActive(true);
            //SceneManager.LoadScene(2);
            //Destroy(player);
            // Set the text value of your 'winText'
            //winTextObject.SetActive(true);
        }
    }

    void loseLife()
    {
        life--;
        SetLifeText();
    }

    void gainLife()
    {
        life++;
        SetLifeText();
    }

    void SpeedUp()
    {
        speed = speedMain * 2;
        //print("speedup" + speed);
    }

    void speedNormal()
    {
        speed = speedMain;
        //print("speednorm" + speed);
    }

    void TurnOffGameObject()
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "EnemyBullet" || other.tag == "Enemy")
        {

            //print("player attacked");

            loseLife();
            //print(other.gameObject.name);

            //CancelInvoke("StartShooting");
            //Invoke("TurnOffGameObject", 0.2f);
            //anim.Play("explosion1");
        }

        if (other.tag == "LifePickup")
        {
            print("life entered");
            gainLife();
        }

        if (other.tag == "SpeedBoost")
        {
            //print("speed entered");
            SpeedUp();
            //CancelInvoke("speedNormal");
            Invoke("speedNormal", 15f);
        }

    }


}
