﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{

    public float min_X = -10.8f, max_X = 10.8f;
    public float min_y = -10.8f, max_y = 10.8f;

    //public bool isY;

    public GameObject asteroidPrefab;
    public GameObject enemyPrefab;

    public float timer = 2f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("spawnEnemies", timer);
    }

    // Update is called once per frame
    void spawnEnemies()
    {
        Vector3 temp = transform.position;

       

        

            float pos_X = Random.Range(min_X, max_X);
            //Vector3 temp = transform.position;
            temp.x = pos_X;

            if (Random.Range(0, 2) > 0)
            {
                Instantiate(asteroidPrefab, temp, Quaternion.Euler(0f, 0f, -98f));
            }
            else
            {
                Instantiate(enemyPrefab, temp, Quaternion.Euler(0f, 0f, -98f));
            }
            Invoke("spawnEnemies", timer);
        
        
    }



}

