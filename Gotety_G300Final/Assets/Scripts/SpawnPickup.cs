﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPickup : MonoBehaviour
{
    public float min_X = -10.8f, max_X = 10.8f;
    public GameObject speedPrefab;
    public GameObject lifePrefab;

    public float timer = 1f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("spawnPickups", timer);
    }

    // Update is called once per frame
    void spawnPickups()
    {
        Vector3 temp = transform.position;





        float pos_X = Random.Range(min_X, max_X);
        //Vector3 temp = transform.position;
        temp.x = pos_X;

        if (Random.Range(0, 2) > 0)
        {
            Instantiate(speedPrefab, temp, Quaternion.Euler(0f, 0f, -98f));
            //print("speed spawn");
        }
        else
        {
            Instantiate(lifePrefab, temp, Quaternion.Euler(0f, 0f, -98f));
            //rint("life spawn");
        }
        Invoke("spawnPickups", timer);


    }



}