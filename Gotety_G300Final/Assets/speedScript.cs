﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedScript : MonoBehaviour
{
    private Player player;

    

    public float speed = 5f;
    public float rotate_speed = 50f;

    float randAngle;
    public float radius;

    Rigidbody2D rb;

    

    public float bound_Y = -11f;

    


    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        
        randAngle = Random.Range(0f, 360f);

        SetPosition();

    }

    // Update is called once per frame
    void Update()
    {
        Move();
        
    }


    void Move()
    {
       

        radius -= speed * Time.deltaTime;
        SetPosition();
    }

    

    void SetPosition()
    {

        float rad = randAngle * Mathf.Deg2Rad;

        Vector3 position = new Vector3(Mathf.Sin(rad), Mathf.Cos(rad), 0f);
        position *= radius;

        // print(position + "angle:" + randAngle);

        rb.MovePosition(position);

        Vector3 euler = new Vector3(0f, 0f, -(randAngle - 180));
        rb.MoveRotation(Quaternion.Euler(euler));


        if (radius <= 0)
            gameObject.SetActive(false);



    }



    void TurnOffGameObject()
    {
        gameObject.SetActive(false);
        //print("puckup off");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            
            Invoke("TurnOffGameObject", 0.5f);
            
        }

    }
}
